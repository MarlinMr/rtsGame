var d = new Date();
var startTime = d.getTime();
var startPrice = 1;
var game = [];
	game.fps = 60;
var ticknr = 0;
var actiualFPS = 1;
var unitConsumption = 1;
var unitBought = false;
var incrementPrice = 1.15;
var id = 0;
var unitList = [];
var counter = 0;
var flipper = false;
var grasCounter = 0;
var unitUpdateTime = 500;
//World
var spritesheet 		= new Image();
	spritesheet.src 	= "./sources/spritesheet.png";
var block,
	blockWidth = 32,
	blockHeight  = blockWidth;
var checked = [[]];
var world = [[]], 
	worldWidth = 50;
	worldHeight = 24;
	worldSize =	worldWidth * worldHeight,
	worldBuildt = false;
var home = {x:Math.floor(worldWidth/2),y:Math.floor(worldHeight/2)};
var gras = [], wood = [], iron = [], gold = [], food = [];
var intGold = 1, intWood = 2, intIron = 3, intFood = 4, intGras = 0;
var populationLimit = 10;
//Wood
var wood = [];
	wood.path = [];
var numberWood 			= 00;
var numberWoodNext 		= 0;
var baseWood 			= 1;
var incrementWood 		= baseWood;
var numberLumberjack 	= 0;
var lastCheckLumberjack = numberLumberjack;
var priceLumberjack 	= startPrice*30;
//Gold
var gold = [];
	gold.path = [];
var numberGold 			= 00;
var numberGoldNext		= 0;
var baseGold 			= 1;
var incrementGold		= baseGold;
var numberGoldMiner		= 0;
var lastCheskGold		= numberGoldMiner;
var priceGoldMiner		= startPrice*60;
var incrementGoldMinerPrice = 1.7;
//Iron
var iron = [];
	iron.path = [];
var numberIron 			= 00;
var numberIronNext 		= 0;
var baseIron 			= 1;
var incrementIron		= baseIron;
var numberIronMiner		= 0;
var lastCheckIron		= numberIronMiner;
var priceIronMiner		= startPrice*45;
//Food
var food = []
	food.path = [];
var numberFood			= 00;
var numberFoodNext		= 0;
var baseFood 			= 1;
var incrementFood		= 5;
var numberFarmer		= 0;
var lastCheckFarmer		= numberFarmer;
var priceFarmer			= startPrice*25;

var start = {x:home.x,y:home.y};	
	
function gameRunner(){
	calcTurn();
	printTurn();
}

game.inerval = setInterval(gameRunner,1000/game.fps);
		
function onLoad(){
	canvas = document.getElementById("GUICanvas");
	canvas.width = worldWidth * blockWidth;
	canvas.height = worldHeight * blockHeight;
	context = canvas.getContext("2d");
	buildWorld();
	detect(start);
}