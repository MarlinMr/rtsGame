//playerCommands

function addMats(numberMat,baseMat){
	numberMat = numberMat + baseMat;
	return numberMat;
}

function upgradeHouse(){
    console.log("Upgrading house");
    world[home.x][home.y] = world[home.x][home.y] + 1;
}

function buyUnit(numberUnit,unitPrice,unitType){
	if(Math.floor(unitPrice) > numberGold ){
		document.getElementById('Message').innerHTML = "You need more Gold!";
		return numberUnit;
	}
	else if(numberFood <= 1){
		document.getElementById('Message').innerHTML = "You need more Food!";
		return numberUnit;
	}
    else if(numberUnits+1 > populationLimit){
		document.getElementById('Message').innerHTML = "Not enough housing!";
		return numberUnit;
	}
	else if(Math.floor(unitPrice) <= numberGold && numberFood > 1){
		numberGold = numberGold - Math.floor(unitPrice);
		unitBought = true;
		createUnit(unitType);
		return (numberUnit +1);
	}
}
