//Draw

//Draw world on load

function drawWorld(){
	if (!worldBuildt){return;}
	var xCord, yCord;
	for (xCord = 0; xCord < worldWidth; xCord++){
		for(yCord = 0; yCord < worldHeight; yCord++){
			var text = String(xCord) + "," + String(yCord); 
			context.drawImage(spritesheet,0,0,blockWidth,blockHeight,xCord*blockWidth,yCord*blockHeight,blockWidth,blockHeight);
            if(world[xCord][yCord]!=0){
                context.drawImage(spritesheet,world[xCord][yCord]*blockWidth,0,blockWidth,blockHeight,xCord*blockWidth,yCord*blockHeight,blockWidth,blockHeight);}
		}
	}
}


//DRAW UNITS

function clearUnit(unit){
    context.drawImage(spritesheet,0,0,blockWidth,blockHeight,unit.x*blockWidth,unit.y*blockHeight,blockWidth,blockHeight);
    if(world[unit.x][unit.y].value >0){
        context.drawImage(spritesheet,world[unit.x][unit.y].value*blockWidth,0,blockWidth,blockHeight,unit.x*blockWidth,unit.y*blockHeight,blockWidth,blockHeight);
    }
    else if(world[unit.x][unit.y]>4){
        context.drawImage(spritesheet,world[unit.x][unit.y]*blockWidth,0,blockWidth,blockHeight,unit.x*blockWidth,unit.y*blockHeight,blockWidth,blockHeight);
    }
}	

function drawUnit(unit){
		context.drawImage(spritesheet,unit.sprite*32,32,blockWidth,blockHeight,unit.x*blockWidth,unit.y*blockHeight,blockWidth,blockHeight);
	}

