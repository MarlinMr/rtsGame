//PathCalculation

function detect(start){
	var x = start.x;
	var y = start.y;
	
	neighbour(start);
	var text;
	if ((!wood[0] || !iron[0] || !gold[0] || !food[0]) && grasCounter<gras.length && 0<=x<worldWidth && 0<=y<worldHeight){
		grasCounter++;
		text = String(x) + "," + String(y);		
		detect({x:gras[grasCounter-1].x,y:gras[grasCounter-1].y});		
		text = String(x) + "," + String(y);
	}
}

function neighbour(start){
	var x = start.x;
	var y = start.y;
	var point = {};
	var N = y - 1,
		S = y + 1,
		E = x + 1,
		W = x - 1,
		result = [];
	if (N>=0){
		if (!checked[x][N]){
			addCheck(x,N,start);
		}
	}
	if (E<worldWidth){
		if (!checked[E][y]){
			addCheck(E,y,start);
		}
	}
	if (S<worldHeight){
		if (!checked[x][S]){
			addCheck(x,S,start);
		}
	}
	if (W>=0){
		if (!checked[W][y]){
			addCheck(W,y,start);
		}
	}    
}

function addCheck(x,y,start){
	point = {x:x,y:y};
	var intMat = world[x][y];
	switch(intMat){
		case intGras:
			gras.push(node(start,point));
			break;
		case intIron:
			iron.push(node(start,point));
			break;
		case intWood:
			wood.push(node(start,point));
			break;
		case intGold:
			gold.push(node(start,point));
			break;
		case intFood:
			food.push(node(start,point));
			break;
	}
	checked[x][y]=true;
}

function node(parent, point){
	var newNode = {
		parent:parent,
		value:point.x + (point.y * worldWidth),
		x:point.x,
		y:point.y,
		f:0,
		g:0
	};
	world[point.x][point.y] = {value:world[point.x][point.y],parent:parent};
	return newNode;
}

function calculatePath(point,path) {
	
	if (point == start){
		path.push(point);
		return path;
	}
	else{
		path = calculatePath(world[point.x][point.y].parent,path);
		path.push(point);
		return path;
	}
}


