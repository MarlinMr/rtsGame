//Units

function createUnit(unitType){
	var unit = {
		type:unitType,
		flipper:false,
		x:(home.x),
		y:(home.y),
		counter:0
	}
	unitList[id] = unit;
	switch (unitType){
		case "lumber":
			wood.path = [];
			wood.path = calculatePath(wood[0],wood.path);
			updateUnit(unitList[id],wood.path,id);
			unit.sprite=0;
			break;
		case "gold":
			gold.path = [];
			gold.path = calculatePath(gold[0],gold.path);
			updateUnit(unitList[id],gold.path,id);
			unit.sprite=1;
			break;
		case "farmer":
			food.path = [];
			food.path = calculatePath(food[0],food.path);
			updateUnit(unitList[id],food.path,id);
			unit.sprite=2;
			break;
		case "iron":
			iron.path = [];
			iron.path = calculatePath(iron[0],iron.path);
			updateUnit(unitList[id],iron.path,id);
			unit.sprite=1;
			break;
	}	
	id=id+1;
}

function updateUnit(unit,path,id){
	setInterval(
		function(){
			clearUnit(unitList[id]);
			if (unit.counter>=path.length){unit.counter--;unit.flipper=true;}
			var x = path[unit.counter].x;
			var y = path[unit.counter].y;
			unit.x=x;
			unit.y=y;
			if(unit.counter < path.length && unit.flipper == false){unit.counter++;}
			else if(unit.counter >= path.length){unit.flipper=true;	unit.counter--;}
			else if(unit.flipper && unit.counter > 0){unit.counter--;}
			else if(unit.flipper && unit.counter >= 0){unit.flipper = false;unit.counter++;}
			unitList[id]=unit;
			drawUnit(unitList[id]);
		},
	unitUpdateTime);
}

