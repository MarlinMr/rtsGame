function buildWorld(){
	worldBuildt = true;
	var xCord, yCord, randomNum;
	var chanceGoldHi = 0.002, 	chanceWoodHi = 0.208, 	chanceIronHi = 0.305, 	chanceFoodHi = 0.45, 	chanceGrasHi = 0;
	var chanceGoldLo = 0, 		chanceWoodLo = 0.2, 	chanceIronLo = 0.3, 	chanceFoodLo = 0.4, 	chanceGrasLo = 0;
	world[Math.floor(worldWidth/2)] = [];
	world[-1] = [];
	for (xCord = 0; xCord < worldWidth; xCord++){
		world[xCord] = [];
		checked[xCord] = [];
		for(yCord = 0; yCord < worldHeight; yCord++){
			randomNum = Math.random();
			checked[xCord][yCord] = false;
			if (world[xCord][yCord-1] == intWood || world[xCord-1][yCord] == intWood ){
				if(randomNum > 0.4 && randomNum < 0.9){
					world[xCord][yCord] = intWood;
				}
				else{
					if (randomNum < chanceGoldHi){
						world[xCord][yCord] = intGold; 
					}
					else if (randomNum > chanceWoodLo && randomNum < chanceWoodHi){
						world[xCord][yCord] = intWood; 
					}
					else if (randomNum > chanceIronLo && randomNum < chanceIronHi){
						world[xCord][yCord] = intIron; 
					}
					else if (randomNum > chanceFoodLo && randomNum < chanceFoodHi){
						world[xCord][yCord] = intFood; 
					}
					else{
						world[xCord][yCord] = intGras;
					}
				}
			}
			else if (randomNum < chanceGoldHi){
				world[xCord][yCord] = intGold;
				}
			else if (randomNum > chanceWoodLo && randomNum < chanceWoodHi){
				world[xCord][yCord] = intWood;
				}
			else if (randomNum > chanceIronLo && randomNum < chanceIronHi){
				world[xCord][yCord] = intIron; 
				}
			else if (randomNum > chanceFoodLo && randomNum < chanceFoodHi){
				world[xCord][yCord] = intFood; 
				}
			else{
				world[xCord][yCord] = intGras;}
		}
	}
    world[home.x][home.y] = 5;
	drawWorld();	
}